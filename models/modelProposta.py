from banco import db
from datetime import datetime


class Proposta(db.Model):
    __tablename__ = 'propostas'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    email = db.Column(db.String(40), nullable=False)
    valor = db.Column(db.Float, nullable=False)
    data_cad = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    proposta_id = db.Column(db.Integer, db.ForeignKey(
        'carros.id'), nullable=False)

    carro = db.relationship('Carro')

    def to_json(self):
        json_propostas = {
            'id': self.id,
            'email': self.email,
            'valor': self.valor,
            'proposta_id': self.proposta_id
        }
        return json_propostas

    @staticmethod
    def from_json(json_propostas):
        email = json_propostas.get('email')
        valor = json_propostas.get('valor')
        proposta_id = json_propostas.get('proposta_id')
        return Proposta(email=email, valor=valor, proposta_id=proposta_id)