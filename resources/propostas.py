from flask import Blueprint, jsonify, request
from banco import db
from models.modelProposta import Proposta
from flask_jwt_extended import jwt_required
from flask_cors import CORS, cross_origin

propostas = Blueprint('propostas', __name__)


@propostas.route('/propostas')
def listagem():
    propostas = Proposta.query.order_by(Proposta.id).all()
    return jsonify([proposta.to_json() for proposta in propostas])


@propostas.route('/propostas/pesq/<palavra>')
def pesquisa(palavra):
    propostas = Proposta.query.order_by(Proposta.id).filter(
        Proposta.email.like(f'%{palavra}%')).all()
    if (len(propostas) == 0):
        return jsonify({"msg": "Modelo não encontrado"}), 400
    else:
        return jsonify([proposta.to_json() for proposta in propostas])

@propostas.route('/propostas', methods=['POST'])
@jwt_required
def inclusao():
    proposta = Proposta.from_json(request.json)
    db.session.add(proposta)
    db.session.commit()
    return jsonify(proposta.to_json()), 201


@propostas.route('/propostas/<int:id>', methods=['DELETE'])
def exclui(id):
    Proposta.query.filter_by(id=id).delete()
    db.session.commit()
    return jsonify({'id': id, 'message': 'Proposta excluída com sucesso'}), 200
